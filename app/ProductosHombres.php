<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosHombres extends Model
{
    protected $table = 'productosHombres';

    protected $fillable = [
        'id', 'nombre', 'precio', 'imagen', 'categoria',
    ];
}
