<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosMujeres extends Model
{
    protected $table = 'productosMujeres';

    protected $fillable = [
        'id', 'nombre', 'precio', 'imagen', 'categoria',
    ];
}
