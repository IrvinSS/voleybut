<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductosComplementos extends Model
{
    protected $table = 'productosComplementos';

    protected $fillable = [
        'id', 'nombre', 'precio', 'imagen', 'categoria',
    ];
}
