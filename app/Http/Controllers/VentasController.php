<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductosMujeres;
use App\ProductosHombres;
use App\ProductosComplementos;

use App\Http\Requests;

class VentasController extends Controller
{

    public function ventaHombres(){
        $productos = ProductosHombres::all();
        return view('ventas/hombres')->with('productos', $productos);
    }

    public function ventaMujeres(){
        $productos = ProductosMujeres::all();
        return view('ventas/mujeres')->with('productos', $productos);
    }

    public function complementos(){
        $productos = ProductosComplementos::all();
        return view('ventas/extras')->with('productos', $productos);
    }

    public function carrito(){
        Cookie::queue($name, $value, $minutes);
        return view('ventas/carrito');
    }
}
