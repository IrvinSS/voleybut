<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function nuevo(){
        return view('ventas/nuevaVenta');
    }

    public function nuevoHombre(){
        return view('ventas/nuevaVentaHombres');
    }

    public function crearHombre(Request $request){
        $nombre = $request->input('nombre');
        $precio = $request->input('precio');
        $imagen = $request->input('imagen');
        $categoria = $request->input('categoria');

        $data = array('nombre'=>$nombre,"precio"=>$precio,"imagen"=>$imagen,"categoria"=>$categoria);

        DB::table('productosHombres')->insert($data);

        echo "Nueva venta creada";

        return redirect('/');
    }

    public function nuevoMujer(){
        return view('ventas/nuevaVentaMujeres');
    }

    public function crearMujer(Request $request){
        $nombre = $request->input('nombre');
        $precio = $request->input('precio');
        $imagen = $request->input('imagen');
        $categoria = $request->input('categoria');

        $data = array('nombre'=>$nombre,"precio"=>$precio,"imagen"=>$imagen,"categoria"=>$categoria);

        DB::table('productosMujeres')->insert($data);

        echo "Nueva venta creada";

        return redirect('/');
    }

    public function nuevoComplemento(){
        return view('ventas/nuevaVentaExtras');
    }

    public function crearComplemento(Request $request){
        $nombre = $request->input('nombre');
        $precio = $request->input('precio');
        $imagen = $request->input('imagen');
        $categoria = $request->input('categoria');

        $data = array('nombre'=>$nombre,"precio"=>$precio,"imagen"=>$imagen,"categoria"=>$categoria);

        DB::table('productosComplementos')->insert($data);

        echo "Nueva venta creada";

        return redirect('/');
    }

    public function eliminar(){
        return view('ventas/eliminar');
    }

    public function eliminarPrenda(){

    }
}
