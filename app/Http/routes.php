<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/perfil', 'HomeController@perfil');



Route::get('/tienda/mujeres', 'VentasController@ventaMujeres');

Route::get('/tienda/hombres', 'VentasController@ventaHombres');

Route::get('/carrito', 'VentasController@carrito');

Route::get('/tienda/extras', 'VentasController@complementos');



Route::get('/tienda/nuevo', 'UsuarioController@nuevo');

Route::get('/tienda/nuevo/hombre', 'UsuarioController@nuevoHombre');

Route::post('/tienda/nuevo/hombre', 'UsuarioController@crearHombre');

Route::get('/tienda/nuevo/mujer', 'UsuarioController@nuevoMujer');

Route::post('/tienda/nuevo/mujer', 'UsuarioController@crearMujer');

Route::get('/tienda/nuevo/extras', 'UsuarioController@nuevoComplemento');

Route::post('/tienda/nuevo/extras', 'UsuarioController@crearComplelmento');

Route::get('/eliminar', 'UsuarioController@eliminar');

Route::post('/eliminar', 'UsuarioController@eliminarPrenda');