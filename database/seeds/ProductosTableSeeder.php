<?php

use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productosHombres')->insert([
            'nombre' => 'Camiseta deportiva',
            'precio' => '200',
            'imagen' => 'https://deporshop.es/media/catalog/product/cache/1/small_image/420x/602f0fa2c1f0d1ba5e241f914e856ff9/r/e/ref07663-0134_1430214033.jpg',
            'categoria' => 'Camisas',
        ]);
        DB::table('productosMujeres')->insert([
            'nombre' => 'Camiseta deportiva',
            'precio' => '260',
            'imagen' => 'https://deporshop.es/media/catalog/product/cache/1/image/535x/602f0fa2c1f0d1ba5e241f914e856ff9/0/7/07259_0027.jpg',
            'categoria' => 'Camisas',
        ]);
        DB::table('productosComplementos')->insert([
            'nombre' => 'Balon',
            'precio' => '170',
            'imagen' => 'http://1.bp.blogspot.com/-VRLC9WHOyR4/Vgv0ZUwt2-I/AAAAAAAAAAs/TpPwF8Ty9-o/s1600/VOLEIBOL%2B-MIKASA-MVA-200.jpg',
            'categoria' => 'Balones',
        ]);
    }
}
