@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">Bienvenido!</div>

                <div class="panel-body" style="text-align: center">
                    @if (Auth::guest())
                        Esta tienda virtual te ofrece todo tipo de contenido relacionado al Volleyball
                        desde ropa deportiva especial para entrenar hasta las mejores pelotas y reds!<br>
                        <form action="{{ url('/register') }}" method="get">
                            <label class="">Crea tu cuenta totalmente gratis!</label><br>
                            <input type="submit" class="btn btn-primary" value="Crear cuenta"
                                   name="crearCuenta" id="btn-register" />
                        </form>
                        <form action="{{ url('/login') }}" method="get">
                            <label class="">¿Ya tienes cuenta?</label><br>
                            <input type="submit" class="btn btn-primary" value="Inicia sesion!"
                                   name="inicioSesion" id="btn-login" />
                        </form>
                    @else
                        Esta tienda virtual te ofrece todo tipo de contenido relacionado al Volleyball
                            desde ropa deportiva especial para entrenar hasta las mejores pelotas y reds!
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
