@extends('layouts.app')

@section('content')
    <style>
        label {
            margin: 2px;
            padding: 2px;
        }
        input {
            padding: 1px;
            margin: 5px;
        }
    </style>
    @if (Auth::user()->admin == 1)
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Agrega un nuevo producto en el catalogo de Mujeres</div>

                        <div class="panel-body" align="center">
                            <form action="{{url('tienda/nuevo/mujer')}}" method="post">
                                {{ csrf_field() }}
                                <div>
                                    <label>Nombre del producto</label><br><input type="text" name="nombre" required>
                                </div>
                                <div>
                                    <label>Precio del producto</label><br><input type="text" name="precio" required>
                                </div>
                                <div>
                                    <label>URL de la imagen del producto</label><br><input type="text" name="imagen" required>
                                </div>
                                <div>
                                    <label>Categoria del producto</label><br><input type="text" name="categoria" required>
                                </div>
                                <div>
                                    <input type="submit" name="submit" class="btn btn-primary" value="Crear nuevo producto">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <meta  http-equiv="refresh" content="0;URL=/">
    @endif

@endsection
