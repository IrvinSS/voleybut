@extends('layouts.app')

@section('content')
    <style>
        table, th, td {
            border: 1px solid black;
            margin: 7px;
            padding: 5px;
            text-align: center;
        }
        img {
            height: 100px;
            width: 100px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Seccion de Hombres</div>

                    <div class="panel-body" align="center">
                        Todo el catalogo de Hombres
                        <table>
                            <thead>
                            <tr>
                                <th> Nombre</th>
                                <th> Precio</th>
                                <th> Imagen</th>
                                <th> <label>Agregar al carrito</label> </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($productos as $producto){ ?>
                            <tr>
                                <td><?php echo $producto->nombre ?></td>
                                <td><?php echo $producto->precio ?></td>
                                <td> <img src="<?php echo $producto->imagen ?>"></td>
                                <td>
                                    <form action="{{url('/carrito')}}" method="get">
                                        <input class="btn btn-success" type="submit" value="Agregar al carrito">
                                        <input type="hidden" name="idProducto" value="<?php echo $producto->id ?>">
                                        <input type="hidden" name="categoria" value="hombres">
                                    </form>
                                    @if (Auth::guest())
                                    @else
                                        @if (Auth::user()->admin == 1)
                                            <form action="{{url('/eliminar')}}" method="get"><br>
                                                <input class="btn btn-danger" type="submit" value="Eliminar">
                                                <input type="hidden" name="idProducto" value="<?php echo $producto->id ?>">
                                                <input type="hidden" name="categoria" value="hombres">
                                            </form>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
