@extends('layouts.app')

@section('content')
    <style>
        table, tr, th {
            margin: 10px;
            padding: 10px;
            align-self: center;
        }
    </style>
    @if (Auth::user()->admin == 1)
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Agrega un nuevo producto a la tienda, escoje a que catalogo tiene que ir</div>
                        <table align="center">
                            <tr>
                                <th>
                                    <form action="{{ url('/tienda/nuevo/hombre') }}" method="get">
                                        <input type="submit" class="btn btn-primary"
                                               name="catalogo" value="Hombres"/>
                                    </form>
                                </th>
                                <th>
                                    <form action="{{ url('/tienda/nuevo/mujer') }}" method="get">
                                        <input type="submit" class="btn btn-primary"
                                               name="catalogo" value="Mujeres"/>
                                    </form>
                                </th>
                                <th>
                                    <form action="{{ url('/tienda/nuevo/extras') }}" method="get">
                                        <input type="submit" class="btn btn-primary"
                                               name="catalogo" value="Complementos"/>
                                    </form>
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @else
        <meta  http-equiv="refresh" content="0;URL=/">
    @endif
@endsection
