@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">Seguro que quires eliminar el producto?</div>

                    <div class="panel-body" align="center">

                        <div class="col-md-6">
                            <form action="{{url('/eliminar')}}" method="post">
                                {{ csrf_field() }}
                                <input class="btn btn-danger" type="submit"value="Si, eliminar">
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form action="{{url('/')}}" method="get">
                                <input class="btn btn-primary" type="submit" value="No, regresar">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
