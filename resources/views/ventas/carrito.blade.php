@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Carrito</div>

                <div class="panel-body">
                    Confirma tu compra para que podamos continuar!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
